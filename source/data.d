module blasterspace.data;

const int BlasterSpace_VERSION_MAJOR = 0;
const int BlasterSpace_VERSION_MINOR = 0;
const int BlasterSpace_VERSION_HOTFIX = 1;

const int WIDTH = 800;
const int HEIGHT = 700;

const float SCALE_X = WIDTH/320;
const float SCALE_Y = HEIGHT/240;

float baseAlienSpawnX = 32 * SCALE_X;
float baseAlienSpawnY = 10 * SCALE_Y;

int enemyCount = 0;
