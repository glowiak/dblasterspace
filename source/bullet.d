module blasterspace.bullet;

import blasterspace.entity;
import blasterspace.main;
import blasterspace.textures;
import blasterspace.data;
import blasterspace.spritemanager;

import raylib;

class Bullet : Entity
{
    this()
    {
        super();
        this.x = plr.x + (11.7 * SCALE_X);
        this.y = plr.y;
        this.texture = TEX.BULLET;

        SpriteManager.add(this);
        SpriteManager.addBullet(this);
    }

    override void loop()
    {
        this.y -= 0.5 + SCALE_Y;
    }
}
