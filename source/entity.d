module blasterspace.entity;

import raylib;

abstract class Entity
{
    float x;
    float y;
    int health;
    int damage;
    Texture2D texture;

    void loop() {}

    this()
    {
    }
}
