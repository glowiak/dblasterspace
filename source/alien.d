module blasterspace.alien;

import std.random;
import std.range;
import std.conv;

import raylib;

import blasterspace.entity;
import blasterspace.textures;
import blasterspace.data;
import blasterspace.spritemanager;
import blasterspace.main;

class Alien : Entity
{
    int moveCount;
    float speed;
    float willShoot;
    int cooldown;
    int skinCooldown;
    int skinNumber;

    this()
    {
        super();

        auto r = new Random(42);
        int rnd1 = uniform(0, 200, r);
        if (rnd1 <= 60)
        { this.skinNumber = 0; } else if (rnd1 > 60 && rnd1 <= 85)
        { this.skinNumber = 1; }

        if (this.skinNumber == 0)
        { this.texture = TEX.ALIEN1; } else if (this.skinNumber == 1)
        { this.texture = TEX.ALIEN2; }

        this.x = baseAlienSpawnX;
        this.y = baseAlienSpawnY;

        if (this.skinNumber == 0) { this.willShoot = 2.0; this.health = 1; } else
        if (this.skinNumber == 1) { this.willShoot = 0.8; this.health = 5; } else
        if (this.skinNumber == 2) { this.willShoot = 1.0; this.health = 15; }

        this.moveCount = 0;
        this.speed = 0.4 * SCALE_X;
        this.damage = 1;

        this.cooldown = 0;
        this.skinCooldown = 0;

        baseAlienSpawnX += 32 * SCALE_X;
        if (baseAlienSpawnX > WIDTH - 32 * SCALE_X)
        {
            baseAlienSpawnX = 32;
            baseAlienSpawnY += 32;
        }

        enemyCount++;
        SpriteManager.add(this);
        SpriteManager.addAlien(this);
    }

    override void loop()
    {
        if (this.y >= plr.y && this.y < 999.0) { plr.health = 0; }

        this.x += this.speed;
        if (this.x >= WIDTH - 32 * SCALE_X)
        {
            this.speed = -0.4 * SCALE_X;
            this.y += 5 * SCALE_Y;
        }
        if (this.x <= 0)
        {
            this.speed = 0.4 * SCALE_X;
            this.x += 5 * SCALE_Y;
        }

        for (int i = 0; i < SpriteManager.bulletCount; i++)
        {
            if (SpriteManager.Bullets[i].x >= this.x && SpriteManager.Bullets[i].x <= this.x + 32 && SpriteManager.Bullets[i].y >= this.y && SpriteManager.Bullets[i].y <= this.y + 32)
            {
                this.health--;
            }
        }

        if (this.health <= 0 && this.y < 999.0)
        {
            plr.score += 250;
            this.y = 999.0;
        }
    }
}
