module blasterspace.textures;

import blasterspace.data;
import raylib;

class Textures
{
    Texture2D BACKGROUND;
    Texture2D PLAYER;
    Texture2D PLAYER_SHOOT;
    Texture2D PLAYER_HIT;
    Texture2D PLAYER_DEAD;
    Texture2D BULLET;
    Texture2D HEART;
    Texture2D ALIEN1;
    Texture2D ALIEN1_1;
    Texture2D ALIEN2;
    Texture2D ALIEN2_1;
    Texture2D ALIEN3;
    Texture2D ALIEN3_1;

    this()
    {
        this.BACKGROUND = LoadTexture("res/bgnd.png");
        this.PLAYER = LoadTexture("res/player.png");
        this.PLAYER_SHOOT = LoadTexture("res/plrshoot.png");
        this.PLAYER_HIT = LoadTexture("res/plrhit.png");
        this.PLAYER_DEAD = LoadTexture("res/plrdead.png");
        this.BULLET = LoadTexture("res/pbullet.png");
        this.HEART = LoadTexture("res/heart.png");
        this.ALIEN1 = LoadTexture("res/enemy1.png");
        this.ALIEN1_1 = LoadTexture("res/enemy1_1.png");
        this.ALIEN2 = LoadTexture("res/enemy2.png");
        this.ALIEN2_1 = LoadTexture("res/enemy2_1.png");
        this.ALIEN3 = LoadTexture("res/enemy3.png");
        this.ALIEN3_1 = LoadTexture("res/enemy3_1.png");

        this.BACKGROUND.width = WIDTH;
        this.BACKGROUND.height = HEIGHT;
        this.PLAYER.width = cast(int)(this.PLAYER.width * SCALE_X);
        this.PLAYER.height = cast(int)(this.PLAYER.height * SCALE_Y);
        this.PLAYER_SHOOT.width = cast(int)(this.PLAYER_SHOOT.width * SCALE_X);
        this.PLAYER_SHOOT.height = cast(int)(this.PLAYER_SHOOT.height * SCALE_Y);
        this.PLAYER_HIT.width = cast(int)(this.PLAYER_HIT.width * SCALE_X);
        this.PLAYER_HIT.height = cast(int)(this.PLAYER_HIT.height * SCALE_Y);
        this.PLAYER_DEAD.width = cast(int)(this.PLAYER_DEAD.width * SCALE_X);
        this.PLAYER_DEAD.height = cast(int)(this.PLAYER_DEAD.height * SCALE_Y);
        this.BULLET.width = cast(int)(this.BULLET.width * SCALE_X);
        this.BULLET.height = cast(int)(this.BULLET.height * SCALE_Y);
        this.HEART.width = cast(int)(this.HEART.width * SCALE_X);
        this.HEART.height = cast(int)(this.HEART.height * SCALE_Y);
        this.ALIEN1.width = cast(int)(this.ALIEN1.width * SCALE_X);
        this.ALIEN1_1.width = cast(int)(this.ALIEN1_1.width * SCALE_X);
        this.ALIEN2.width = cast(int)(this.ALIEN2.width * SCALE_X);
        this.ALIEN2_1.width = cast(int)(this.ALIEN2_1.width * SCALE_X);
        this.ALIEN3.width = cast(int)(this.ALIEN3.width * SCALE_X);
        this.ALIEN3_1.width = cast(int)(this.ALIEN3_1.width * SCALE_X);
    }
}

Textures TEX;

void loadTextures()
{
    TEX = new Textures();
}
