module blasterspace.state;

class State
{
    static const int MENU = 0;
    static const int OPTIONS = 1;
    static const int ABOUT = 2;
    static const int GAME = 3;

    static int CURRENT = State.GAME;
}
