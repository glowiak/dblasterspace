module blasterspace.spritemanager;

import blasterspace.entity;
import blasterspace.bullet;
import blasterspace.alien;

class SpriteManager
{
    static int count = 0;
    static Entity[] Sprite = new Entity[999];

    static int alienCount = 0;
    static Alien[] Aliens = new Alien[999];

    static int bulletCount = 0;
    static Bullet[] Bullets = new Bullet[999];

    static void add(Entity sprite)
    {
        Sprite[count] = sprite;
        count++;
    }
    static void addAlien(Alien alien)
    {
        Aliens[alienCount] = alien;
        alienCount++;
    }
    static void addBullet(Bullet bul)
    {
        Bullets[bulletCount] = bul;
        bulletCount++;
    }
    static void clear()
    {
        count = 0;
        alienCount = 0;
        bulletCount = 0;
        Sprite = new Entity[999];
        Aliens = new Alien[999];
        Bullets = new Bullet[999];
    }
}
