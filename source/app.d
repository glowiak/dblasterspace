module blasterspace.main;

import std.stdio;
import raylib;

import blasterspace.data;
import blasterspace.player;
import blasterspace.spritemanager;
import blasterspace.textures;
import blasterspace.bullet;
import blasterspace.state;
import blasterspace.alien;

Player plr;

void main()
{
    float bgy1 = -HEIGHT;
    float bgy2 = 0;

    InitWindow(WIDTH, HEIGHT, "BlasterSpaceD v0.0.1");
    SetTargetFPS(315);

    loadTextures();
    plr = new Player();

    SpriteManager.add(plr);

    new Alien();
    new Alien();

    while (!WindowShouldClose())
    {

        bgy1 += 0.2 * SCALE_Y;
        bgy2 += 0.2 * SCALE_Y;

        if (bgy1 >= HEIGHT) { bgy1 = -HEIGHT; }
        if (bgy2 >= HEIGHT) { bgy2 = -HEIGHT; }
       
        switch (State.CURRENT)
        {
            case State.GAME:
            {
                if (IsKeyDown(KeyboardKey.KEY_LEFT) && plr.health > 0) { plr.move(-0.4 * SCALE_X); }
                if (IsKeyDown(KeyboardKey.KEY_RIGHT) && plr.health > 0) { plr.move(0.4 * SCALE_X); }

                if (IsKeyDown(KeyboardKey.KEY_SPACE) && plr.health > 0) { plr.isShooting = true; } else { plr.isShooting = false; }

                for (int i = 0; i < SpriteManager.count; i++)
                {
                    SpriteManager.Sprite[i].loop();
                }
            } break;
            default: {} break;
        }

        BeginDrawing();
        ClearBackground(Colors.BLACK);

        switch (State.CURRENT)
        {
            case State.GAME:
            {
                DrawTexture(TEX.BACKGROUND, 0, cast(int)bgy1, Colors.WHITE);
                DrawTexture(TEX.BACKGROUND, 0, cast(int)bgy2, Colors.WHITE);

                for (int i = 0; i < SpriteManager.count; i++)
                {
                    DrawTexture(SpriteManager.Sprite[i].texture, cast(int)SpriteManager.Sprite[i].x, cast(int)SpriteManager.Sprite[i].y, Colors.WHITE);
                }

                int base_x = 1;
                for (int i = 0; i < plr.health; i++)
                {
                    DrawTexture(TEX.HEART, base_x, cast(int)(15 * SCALE_Y), Colors.WHITE);
                    base_x += cast(int)(10 * SCALE_X);
                }
                DrawText(TextFormat("%d", plr.score), 5, 1, 25, Colors.YELLOW);
            } break;
            default: {} break;
        }

        EndDrawing();
    }

    CloseWindow();
}
