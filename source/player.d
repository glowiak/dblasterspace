module blasterspace.player;

import raylib;

import blasterspace.entity;
import blasterspace.data;
import blasterspace.textures;
import blasterspace.bullet;

class Player : Entity
{
    bool isShooting;
    int cooldown;
    int score;

    this()
    {
        super();
        this.x = WIDTH/2 - cast(int)(32 * SCALE_X);
        this.y = HEIGHT - cast(int)(32 * SCALE_Y) + 10;
        this.health = 4;
        this.damage = 1;
        this.texture = TEX.PLAYER;
        this.cooldown = 0;
        this.isShooting = false;
        this.score = 0;
    }

    void move(float much)
    {
        if (much < 0 && this.x > 0 || much > 0 && this.x < WIDTH - cast(int)(32 * SCALE_X))
        {
            this.x += much;
        }
    }

    override void loop()
    {
        if (this.isShooting && this.cooldown == 0)
        {
            this.texture = TEX.PLAYER_SHOOT;
            new Bullet();
            this.cooldown = 50;
        }
        if (this.cooldown <= 35) { this.texture = TEX.PLAYER; }
        if (this.cooldown > 0) { this.cooldown--; }
    }
}
