module blasterspace.particle;

import blasterspace.entity;
import blasterspace.spritemanager;

class Particle : Entity
{
    float speedx;
    float speedy;

    this(float speedx, float speedy, float x, float y)
    {
        super();

        this.x = x;
        this.y = y;

        this.speedx = speedx;
        this.speedy = speedy;
    }

    override void loop()
    {
    }
}
