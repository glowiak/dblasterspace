#!/bin/sh
dub clean
dub build

rm -rf blasterspace-d
mkdir -p blasterspace-d

cp -rfv ./res blasterspace-d/res

cp -v ./dblasterspace blasterspace-d/game

strip blasterspace-d/game

tar czvf blasterspace-0.0.1-$(uname)_$(uname -m).tar.gz blasterspace-d/

rm -rfv blasterspace-d/
